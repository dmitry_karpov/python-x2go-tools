#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""X2GO Broker.

Usage:
  broker.py [--user=<USERNAME>] [--task=<TASK>] [--sid=<SID>]

"""

from docopt import docopt

TASKS=['listsessions', 'selectsession']


def list_sessions(c, args):
    pass


def select_session(c, args):
    pass


if __name__ == '__main__':
    arguments = docopt(__doc__, version='X2GO Broker')
    if arguments['--task'] not in TASKS:
        raise Exception
    print(arguments)