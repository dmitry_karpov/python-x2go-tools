#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""X2GO Discovery.

Usage:
  x2go-discovery.py (--etcd | --consul)
                    [--cert-file=<file>] [--key-file=<file>] [--ca-file=<file>]
                    [--username=<auth> | --token=<uuid>]
                    [--peers=<peers>]

Options:
  --etcd|--consul           The kvstore to connect to. Can be consul or etcd [default: --etcd]
  --cert-file=<file>        Identify HTTPS client using this SSL certificate file.
  --key-file=<file> 	    Identify HTTPS client using this SSL key file.
  --ca-file=<file>          Verify certificates of HTTPS-enabled servers using this CA bundle.
  --username=<auth>         ETCD: provide username:password.
  --token=<uuid>            CONSUL: ACL tokens to operate requests.
  --peers=<peers>           The url of the kvstore. (eg. http://127.0.0.1:4001 or localhost:8500)
                            [default: http://127.0.0.1:4001]

"""

from docopt import docopt
from docker import Client
import common
import kvlib
import signal
from procwrapper import RepeatableTimer


class X2GoSessionChecker(object):
    _COMMAND = 'x2golistsessions_root'

    def __init__(self, id=None, docker_cli=None, ttl=10, update_callback=None):
        self._container_id = id
        self._check_ttl = ttl
        self._timer = RepeatableTimer(10, self._x2go_session_checker)
        self._docker_cli = docker_cli
        self._kv_updater = update_callback
        data = self._docker_cli.inspect_container(id)
        self._container_ip = data['NetworkSettings']['IPAddress']
        # TODO: Move FORCED_USER to labels
        self._container_user = dict([(kv[:kv.find('=')], kv[kv.find('=') + 1:]) for kv in data['Config']['Env']])[
            'FORCED_USER']

    def _restart_timer_after_check(func):
        def wrapper(self):
            func(self)
            self._timer.cancel()
            self._timer.start()

        return wrapper

    @_restart_timer_after_check
    def _x2go_session_checker(self):
        self._kv_updater(
                raw_sessions=self._docker_cli.exec_start(
                        self._docker_cli.exec_create(
                                self._container_id,
                                X2GoSessionChecker._COMMAND
                        )
                ),
                params={
                    'user': self._container_user,
                    'ip': self._container_ip
                },
                ttl=self._check_ttl
        )

    def start(self):
        print('Starting worker (user: %s, ip: %s): %s' % (
            self._container_user,
            self._container_ip,
            self._container_id
        ))
        self._x2go_session_checker()
        return self

    def stop(self):
        self._timer.cancel()
        print('Stopping worker: %s' % self._container_id)
        return self


class KVStore(kvlib.Store):
    def __init__(self, backend=kvlib.ETCD, *args, **kwargs):
        self._kv = kvlib.new_store(backend, *args, **kwargs)

    def __getattr__(self, item):
        print("getattr")
        return getattr(self._kv, item)

    def x2go_session_update(self, raw_sessions, params, ttl=10):
        if self._kv.exists(common.X2GoSession.SESSION_PATH.format(**params)):
            # TODO: Check and Update
            self._kv.put(
                    common.X2GoSession.SESSION_PATH.format(**params),
                    raw_sessions,
                    ttl=ttl * 2,
            )
        else:
            self._kv.put(
                    common.X2GoSession.SESSION_PATH.format(**params),
                    raw_sessions,
                    ttl=ttl * 2,
            )
            # TODO: update if session has changed

    def x2go_session_remove(self):
        pass


class Watchtower(object):
    _CONSUL_DEFAULTS = {
        'host': '127.0.0.1',
        'port': 8500,
    }

    def __init__(self, arguments):
        self._docker_cli = Client(base_url='unix://var/run/docker.sock', version='auto')
        self._events_flag = True

        if arguments['--etcd']:
            self._kv = KVStore(
                    kvlib.ETCD,
                    **next(
                            common.get_kv_peer(
                                    arguments['--peers']
                            )
                    )
            )
        elif arguments['--consul']:
            # TODO: fix consul initialization
            raise NotImplementedError('Consul initialization')
            self._kv = KVStore(kvlib.CONSUL, **Watchtower._CONSUL_DEFAULTS)
        else:
            raise Exception('Select k/v backend')

        self._workers = {
            container['Id']: X2GoSessionChecker(
                    container['Id'],
                    docker_cli=self._docker_cli,
                    update_callback=self._kv.x2go_session_update
            ).start() for container in self._get_running_containers()}

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        [self._workers[id].stop() for id in self._workers.keys()]

    @staticmethod
    def _check_container_type(container):
        return True if 'X2GO_TYPE' in container['Labels'] and container['Labels']['X2GO_TYPE'] == 'x2go-session' \
            else False

    def _get_running_containers(self):
        return filter(
                lambda c: Watchtower._check_container_type(c),
                self._docker_cli.containers(
                        filters={'status': 'running'}
                )
        )

    def watch(self):
        for event in self._docker_cli.events(decode=True):
            if event['status'] == 'start':
                data = self._docker_cli.inspect_container(event['id'])
                if 'X2GO_TYPE' in data['Config']['Labels'] and Watchtower._check_container_type(data['Config']):
                    self._workers.update(
                            {
                                event['id']: X2GoSessionChecker(
                                        event['id'],
                                        docker_cli=self._docker_cli,
                                        update_callback=self._kv.x2go_session_update
                                ).start()
                            }
                    )
            elif event['status'] == 'die' and event['id'] in self._workers:
                self._workers.pop(event['id']).stop()

            if not self._events_flag:
                break
        return self

    def sigterm_handler(self, *args, **kwargs):
        print("SIGTERM handler")
        self._events_flag = False
        # TODO: With this need to do something
        container = self._docker_cli.create_container(
                image='intersoftlab/x2go-base:latest',
                command='false'
        )


if __name__ == '__main__':
    arguments = docopt(__doc__, version='X2GO Discovery')
    if arguments['--ca-file'] or arguments['--cert-file'] or arguments['--consul'] or arguments['--key-file'] or \
            arguments['--token'] or arguments['--username']:
        print(arguments)
        raise NotImplementedError('Not supported')
    with Watchtower(arguments) as discovery:
        signal.signal(signal.SIGTERM, discovery.sigterm_handler)
        signal.signal(signal.SIGINT, discovery.sigterm_handler)
        discovery.watch()
