# -*- coding: utf-8 -*-
from urllib.parse import urlparse
from itertools import zip_longest
import re

class X2GoSession(object):
    SESSION_PATH = 'x2go/sessions/{user}/{ip}'
    _X2GO_SESSION_ATTR=[
        'agent_pid',
        'session_id',
        'port','host',
        'state',
        'init_time',
        'session_cookie',
        'client_ip',
        'gr_port',
        'snd_port',
        'last_time',
        'user',
        'age_in_secs',
        'sshfs_port'
    ]

    def __init__(self):
        self._data = None

    def loads(self, s):
        self._data = s
        return self

    def loadd(self, dictionary):
        self._data = '|'.join(
            map(
                lambda key: dictionary[key],
                X2GoSession._X2GO_SESSION_ATTR
            )
        )+'|'
        return self

    def dumps(self):
        return self._data

    def dumpd(self):
        if self._data:
            return zip_longest(X2GoSession._X2GO_SESSION_ATTR, self._data.strip('|').split('|'),fillvalue='')
        else:
            return None


def get_kv_peer(peers):
    regex = re.compile(
        r'^(?:http)s?://'
    )
    res = re.match(regex, peers)
    scheme = res.group() if res else ''
    for peer in peers.lstrip(scheme).split(','):
        config = dict(
                zip(['host', 'port'], peer.split(':'))
        )
        if scheme:
            config.update({'protocol': scheme.rstrip('://')})
        yield config
